package modelo;
public class Bomba {
    private int numDeBomba;
    private float capacidadDeLaBomba;
    private float acumuladorSeLitros;
    private Gasolina gasolina;
    
    public Bomba(){
        this.acumuladorSeLitros=0;
        this.capacidadDeLaBomba=0;
        this.numDeBomba=0;
        this.gasolina = new Gasolina();
    }
    
    public Bomba(int numDeBomba,float capacidadDeLaBomba,float acumuladorSeLitros,Gasolina gasolina){
        this.numDeBomba = numDeBomba;
        this.capacidadDeLaBomba = capacidadDeLaBomba;
        this.acumuladorSeLitros = acumuladorSeLitros;
        this.gasolina = gasolina;
    }
            
    public Bomba(Bomba B){
        this.acumuladorSeLitros= B.acumuladorSeLitros;
        this.capacidadDeLaBomba= B.capacidadDeLaBomba;
        this.numDeBomba= B.numDeBomba;
        this.gasolina= B.gasolina;
    }
    public int getNumDeBomba() {
        return numDeBomba;
    }

    public void setNumDeBomba(int numDeBomba) {
        this.numDeBomba = numDeBomba;
    }

    public float getCapacidadDeLaBomba() {
        return capacidadDeLaBomba;
    }

    public void setCapacidadDeLaBomba(float capacidadDeLaBomba) {
        this.capacidadDeLaBomba = capacidadDeLaBomba;
    }

    public float getAcumuladorSeLitros() {
        return acumuladorSeLitros;
    }

    public void setAcumuladorSeLitros(float acumuladorSeLitros) {
        this.acumuladorSeLitros = acumuladorSeLitros;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    public void IniciarBomba(int IdBomba,Gasolina gasolina){
        this.gasolina = gasolina;
        this.capacidadDeLaBomba = 200.0f;
        this.acumuladorSeLitros = 0.0f;
        this.numDeBomba = IdBomba;
    }
    
    public float InventarioGasolina(){
        return capacidadDeLaBomba-acumuladorSeLitros;
    }
    
    public float VenderGasolina(float cantidad){
        this.acumuladorSeLitros += cantidad;
        return cantidad * this.gasolina.getPrecio();
        
    }

    public float VentasTotales(){
        return this.acumuladorSeLitros * this.gasolina.getPrecio();
    }
}