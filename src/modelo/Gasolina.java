package modelo;
public class Gasolina {
    private String IdGasolina;
    private String tipo;
    private float precio;
    
    public Gasolina(){
        this.precio=0;
        this.IdGasolina="";
        this.tipo="";
    }
    
    public Gasolina(String IdGasolina,String tipo,float precio){
        this.IdGasolina = IdGasolina;
        this.precio = precio;
        this.tipo = tipo;
    }
    
    public Gasolina(Gasolina G){
        this.IdGasolina = G.IdGasolina;
        this.precio = G.precio;
        this.tipo = G.tipo;
    }

    public String getIdGasolina() {
        return IdGasolina;
    }

    public void setIdGasolina(String IdGasolina) {
        this.IdGasolina = IdGasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
}
